package br.com.virtual.sql.mysql;

/**
 * Created by vinic on 11/10/2017.
 */

public class MySQLException extends Exception {

    private static final long serialVersionUID = 1149241039409861914L;

    // constrói um objeto NumeroNegativoException com a mensagem passada por parâmetro
    public MySQLException(String msg){
        super(msg);
    }

    public MySQLException(String msg, Throwable cause){
        super(msg, cause);
    }

}
