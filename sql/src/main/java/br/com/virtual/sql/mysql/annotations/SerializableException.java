package br.com.virtual.sql.mysql.annotations;

/**
 * Created by vinic on 11/10/2017.
 */

public class SerializableException extends Exception {

    private static final long serialVersionUID = 1149244033407861914L;

    // constrói um objeto NumeroNegativoException com a mensagem passada por parâmetro
    public SerializableException(String msg){
        super(msg);
    }

    public SerializableException(String msg, Throwable cause){
        super(msg, cause);
    }
}
