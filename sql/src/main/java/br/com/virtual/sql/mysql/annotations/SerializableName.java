package br.com.virtual.sql.mysql.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by vinic on 11/10/2017.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface SerializableName {

    public String coluna() default "N/A";

}
