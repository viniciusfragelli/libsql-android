package br.com.virtual.sql.mysql;

import android.util.Log;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.virtual.sql.mysql.annotations.SerializableException;
import br.com.virtual.sql.mysql.annotations.SerializableName;
import br.com.virtual.sql.mysql.utils.BancoInformation;


/**
 * Created by vinic on 11/10/2017.
 */

public class MySQL {

    private Connection conn;
    private Statement st;
    private ResultSet rs;

    private List<HashMap<String,Object>> map;
    private int linha_atual = -1;

    private static BancoInformation info;
    private BancoInformation infoLocal = null;
    private String TAG = "MySQL-Debug";

    public boolean AUTO_CONECT_CLOSE = true;
    public long TIMEOUT = 10000;
    public boolean isDEBUG = false;
    private String url;
    public MySQLException isError = null;
    private boolean sucesso = false;

    public MySQL(BancoInformation info) {
        this.infoLocal = info;
        preparaURLConexao();
    }

    public MySQL(){
        preparaURLConexao();
    }

    public void setDEBUG(boolean debug){
        isDEBUG = debug;
    }

    public void setTimeout(long time){
        TIMEOUT = time;
        preparaURLConexao();
    }

    public void setAutoConectAndClose(boolean is){
        AUTO_CONECT_CLOSE = is;
    }

    private void preparaURLConexao(){
        String serverName = (infoLocal != null) ? infoLocal.getDNS() : info.getDNS();
        String mydatabase = (infoLocal != null) ? infoLocal.getBase() : info.getBase();
        url = "jdbc:mysql://" + serverName + "/" + mydatabase+"?connectTimeout="+TIMEOUT;
    }

    public static void setBancoInformation(BancoInformation banco){
        info = banco;
    }

    public Exception getError(){
        return isError;
    }

    public boolean isError(){
        return (isError != null);
    }

    public void select(String sql) throws MySQLException {
        if(isDEBUG) Log.d(TAG,"SQL: "+sql);
        sucesso = false;
        if(AUTO_CONECT_CLOSE) conectaBanco();
        if(isConectado()){
            try {
                rs = st.executeQuery(sql);
                carregaEmMemoriaResultado();
                sucesso = true;
            } catch (SQLException e) {
                if(isDEBUG)e.printStackTrace();
                isError = new MySQLException(e.getMessage());
            }finally {
                if(AUTO_CONECT_CLOSE) fechaConexaoBanco();
            }
            if(isError != null)throw isError;
        }
    }

    public void callNonReturn(String sql) throws MySQLException {
        if(isDEBUG) Log.d(TAG,"SQL: "+sql);
        sucesso = false;
        if(AUTO_CONECT_CLOSE)conectaBanco();
        if(isConectado()){
            try {
                st.execute(sql);
                sucesso = true;
            } catch (SQLException e) {
                if(isDEBUG)e.printStackTrace();
                isError = new MySQLException(e.getMessage());
            }finally {
                if(AUTO_CONECT_CLOSE)fechaConexaoBanco();
            }
            if(isError != null)throw isError;
        }
    }

    public <T> List<T> getArrayObjetosVO(Class klass) throws MySQLException, IllegalAccessException, SerializableException, InstantiationException {
        List<T> lista = new ArrayList<>();
        while(next()){
            lista.add((T) getObjectVO(klass));
        }
        return lista;
    }

    private void mostraColunas(List<String> lista){
        Log.d(TAG,"------- Colunas -------");
        for(int i = 0; i < lista.size(); i++){
            Log.d(TAG,"Coluna "+(i+1)+": "+lista.get(i));
        }
    }

    public Object getObjectVO(Class klass) throws IllegalAccessException, InstantiationException, MySQLException, SerializableException {
        Object ob = klass.newInstance();
        for(Field field : klass.getDeclaredFields()){
            String coluna = field.getName();
            SerializableName serializableName = null;
            field.setAccessible(true);
            if(field.isAnnotationPresent(SerializableName.class)) {
                serializableName = (SerializableName) field.getAnnotation(SerializableName.class);
                if(serializableName.coluna() != null){
                    if(!serializableName.coluna().equalsIgnoreCase("N/A")){
                        coluna = serializableName.coluna();
                    }
                }
            }
            if(isDEBUG)Log.d(TAG,"Field do VO: "+coluna+" - Type: "+field.getType());
            if(linha_atual == -1){
                isError = new MySQLException("Index read line -1 not exists, please use function next before this function");
                throw isError;
            }
            if(map.get(linha_atual).containsKey(coluna)) {
                if(field.getType().isAssignableFrom(String.class)){
                    field.set(ob,getString(coluna));
                }else if(field.getType().isAssignableFrom(float.class)){
                    if(getFloat(coluna) == null){
                        throw new MySQLException("Formato incopativel com a conversão! float");
                    }else{
                        field.setFloat(ob,getFloat(coluna));
                    }
                }else if(field.getType().isAssignableFrom(int.class)){
                    if(getInt(coluna) == null){
                        throw new MySQLException("Formato incopativel com a conversão! int");
                    }else {
                        field.setInt(ob, getInt(coluna));
                    }
                }else if(field.getType().isAssignableFrom(boolean.class)) {
                    if(getBoolean(coluna) == null){
                        throw new MySQLException("Formato incopativel com a conversão! boolean");
                    }else {
                        field.setBoolean(ob, getBoolean(coluna));
                    }
                }else if(field.getType().isAssignableFrom(Integer.class)){
                    field.set(ob,getInt(coluna));
                }else if(field.getType().isAssignableFrom(Boolean.class)){
                    field.set(ob,getBoolean(coluna));
                }else if(field.getType().isAssignableFrom(Float.class)){
                    field.set(ob,getFloat(coluna));
                }else if(field.getType().isAssignableFrom(double.class)){
                    if(getDouble(coluna) == null){
                        throw new MySQLException("Formato incopativel com a conversão! double");
                    }else {
                        field.set(ob, getDouble(coluna));
                    }
                }else if(field.getType().isAssignableFrom(Double.class)){
                    field.set(ob, getDouble(coluna));
                }else if(field.getType().isAssignableFrom(java.util.Date.class)) {
                    field.set(ob,getDate(coluna));
                }else if(field.getType().isAssignableFrom(Time.class)) {
                    field.set(ob,getTime(coluna));
                }else if(field.getType().isAssignableFrom(java.sql.Date.class)) {
                    field.set(ob,getDateSQL(coluna));
                }else if(field.getType().toString().equals("class [B")){
                    field.set(ob,getBytes(coluna));
                }else{
                    throw new MySQLException("Formato não existente! field: "+field.getName()+" - type_field: "+field.getType());
                }
            }else{
                if(serializableName != null){
                    throw new SerializableException("coluna não encontrada "+coluna);
                }
            }
        }
        return ob;
    }

    public void callWithReturn(String sql) throws MySQLException {
        if(isDEBUG) Log.d(TAG,"SQL: "+sql);
        sucesso = false;
        if(AUTO_CONECT_CLOSE)conectaBanco();
        if(isConectado()){
            try {
                rs = st.executeQuery(sql);
                carregaEmMemoriaResultado();
                sucesso = true;
            } catch (SQLException e) {
                if(isDEBUG)e.printStackTrace();
                isError = new MySQLException(e.getMessage());
            }finally {
                if(AUTO_CONECT_CLOSE)fechaConexaoBanco();
            }
            if(isError != null)throw isError;
        }
    }

    public void update(String sql) throws MySQLException {
        if(isDEBUG) Log.d(TAG,"SQL: "+sql);
        sucesso = false;
        if(AUTO_CONECT_CLOSE)conectaBanco();
        if(isConectado()){
            try {
                st.execute(sql);
                sucesso = true;
            } catch (SQLException e) {
                if(isDEBUG)e.printStackTrace();
                isError = new MySQLException(e.getMessage());
            }finally {
                if(AUTO_CONECT_CLOSE)fechaConexaoBanco();
            }
            if(isError != null)throw isError;
        }
    }

    public void delete(String sql) throws MySQLException {
        if(isDEBUG) Log.d(TAG,"SQL: "+sql);
        sucesso = false;
        if(AUTO_CONECT_CLOSE)conectaBanco();
        if(isConectado()){
            try {
                st.execute(sql);
                sucesso = true;
            } catch (SQLException e) {
                if(isDEBUG)e.printStackTrace();
                isError = new MySQLException(e.getMessage());
            }finally {
                if(AUTO_CONECT_CLOSE)fechaConexaoBanco();
            }
            if(isError != null)throw isError;
        }
    }

    public void clearConsulta(){
        map = null;
    }

    public boolean isConsultaSucesso(){
        boolean aux = sucesso;
        sucesso = false;
        return aux;
    }

    public boolean haveConsultaArmazenada(){
        if(map != null){
            return true;
        }else{
            return false;
        }
    }

    public boolean fechaConexao(){
        if(isConectado()){
            if(!AUTO_CONECT_CLOSE){
                fechaConexaoBanco();
                return isConectado();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    private void fechaConexaoBanco(){
        if(conn != null){
            try {
                conn.close();
                conn = null;
            } catch (SQLException e) {
                e.printStackTrace();
                isError = new MySQLException(e.getMessage());
            }
        }
    }

    public boolean conecta(){
        if(!AUTO_CONECT_CLOSE){
            try {
                conectaBanco();
                return isConectado();
            } catch (MySQLException e) {
                isError = new MySQLException(e.getMessage());
                if(isDEBUG)e.printStackTrace();
                return false;
            }
        }else{
            return false;
        }
    }

    public void conectaBanco() throws MySQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection(this.url, (infoLocal != null) ? infoLocal.getUser() : info.getUser(), (infoLocal != null) ? infoLocal.getPass() : info.getPass());
            st = conn.createStatement();
        } catch (SQLException e) {
            isError = new MySQLException(e.getMessage());
            e.printStackTrace();
            conn = null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            isError = new MySQLException(e.getMessage());
            conn = null;
        }
        if(isError != null)throw isError;
    }

    public boolean isConectado(){
        return (conn != null);
    }

    private void carregaEmMemoriaResultado() throws SQLException {
        List<String> colunas = new ArrayList<>();
        ResultSetMetaData data = rs.getMetaData();
        for (int i = 1; i < data.getColumnCount()+1; i++) {
            colunas.add(data.getColumnLabel(i));
        }
        if(isDEBUG)mostraColunas(colunas);
        //for(int i = 1; i < data.getColumnCount()+1; i++)
        map = new ArrayList<>();
        while(rs.next()){
            HashMap<String,Object> linha = new HashMap<>();
            for(int i = 0; i < colunas.size(); i++){
                linha.put(colunas.get(i), rs.getObject(i+1));
            }
            map.add(linha);
        }
        linha_atual = -1;
    }

    public boolean next() throws MySQLException {
        if(map == null) throw new MySQLException("ResultSet não foi inicializada!");
        linha_atual++;
        if(linha_atual < map.size()){
            return true;
        }else{
            return false;
        }
    }

    public Integer getInt(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        try{
            if(map.get(linha_atual).get(coluna) == null){
                return null;
            }else {
                return Integer.parseInt(map.get(linha_atual).get(coluna) + "");
            }
        }catch(NumberFormatException ex){
            throw new MySQLException("Formato solicitado da coluna "+coluna+" invalido!");
        }
    }

    public Boolean getBoolean(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        try{
            if(map.get(linha_atual).get(coluna) == null){
                return null;
            }else{
                String aux = map.get(linha_atual).get(coluna)+"";
                if(aux.equals("1") || aux.equalsIgnoreCase("true")){
                    return true;
                }else{
                    return false;
                }
            }
        }catch(NumberFormatException ex){
            throw new MySQLException("Formato solicitado da coluna "+coluna+" invalido!");
        }
    }

    public byte [] getBytes(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        if(map.get(linha_atual).get(coluna) == null){
            return null;
        }else{
            return (byte[]) map.get(linha_atual).get(coluna);
        }
    }

    public Double getDouble(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        try{
            if(map.get(linha_atual).get(coluna) == null){
                return null;
            }else{
                return Double.parseDouble(map.get(linha_atual).get(coluna)+"");
            }
        }catch(NumberFormatException ex){
            throw new MySQLException("Formato solicitado da coluna "+coluna+" invalido!");
        }
    }

    public Object getObject(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        try{
            return map.get(linha_atual).get(coluna);
        }catch(NumberFormatException ex){
            throw new MySQLException("Formato solicitado da coluna "+coluna+" invalido!");
        }
    }

    public java.util.Date getDate(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        try{
            try {
                return new SimpleDateFormat("yyyy-MM-dd").parse(map.get(linha_atual).get(coluna).toString());
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }catch(NumberFormatException ex){
            throw new MySQLException("Formato solicitado da coluna "+coluna+" invalido!");
        }
    }

    public java.sql.Date getDateSQL(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        try{
            try {
                return new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse(map.get(linha_atual).get(coluna).toString()).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }catch(NumberFormatException ex){
            throw new MySQLException("Formato solicitado da coluna "+coluna+" invalido!");
        }
    }


    public Time getTime(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        try{
            try {
                return new Time(new SimpleDateFormat("HH:mm:ss").parse(map.get(linha_atual).get(coluna).toString()).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }catch(NumberFormatException ex){
            throw new MySQLException("Formato solicitado da coluna "+coluna+" invalido!");
        }
    }

    public String getString(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        try{
            if(map.get(linha_atual).get(coluna) == null){
                return null;
            }else {
                return map.get(linha_atual).get(coluna) + "";
            }
        }catch(NumberFormatException ex){
            throw new MySQLException("Formato solicitado da coluna "+coluna+" invalido!");
        }
    }

    public Float getFloat(String coluna) throws MySQLException {
        verificaLinhaLida();
        verificaSeExisteColuna(coluna);
        try{
            if(map.get(linha_atual).get(coluna) == null){
                return null;
            }else {
                return Float.parseFloat(map.get(linha_atual).get(coluna) + "");
            }
        }catch(NumberFormatException ex){
            throw new MySQLException("Formato solicitado da coluna "+coluna+" invalido!");
        }
    }

    private void verificaSeExisteColuna(String coluna) throws MySQLException {
        if(!map.get(linha_atual).containsKey(coluna)){
            throw new MySQLException("Coluna "+coluna+" não existe");
        }
    }

    private void verificaLinhaLida() throws MySQLException {
        if(linha_atual == -1 || map == null){
            throw new MySQLException("ResultSet não foi inicializada!");
        }
        if(linha_atual >= map.size()){
            throw new MySQLException("Linha acessada não existe pois ResultSet esgotou!");
        }
    }

}
