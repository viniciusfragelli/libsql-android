package br.com.virtual.sql.mysql.utils;

/**
 * Created by vinic on 11/10/2017.
 */

public abstract class BancoInformation {

    public abstract String getUser();

    public abstract String getPass();

    public abstract String getDNS();

    public abstract String getBase();
}
