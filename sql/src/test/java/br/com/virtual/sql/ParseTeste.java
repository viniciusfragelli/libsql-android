package br.com.virtual.sql;

import java.sql.Date;
import java.sql.Time;

import br.com.virtual.sql.mysql.annotations.SerializableName;

/**
 * Created by vinic on 02/11/2017.
 */

public class ParseTeste {

    @SerializableName(coluna = "times")
    public Time times;

    @SerializableName(coluna = "dates")
    public java.util.Date date;

    @SerializableName(coluna = "dates")
    public java.sql.Date dateSQL;

    @SerializableName(coluna = "timestamps")
    public java.util.Date dataToTimeStamp;

    @SerializableName(coluna = "datetime")
    public java.util.Date dataToDateTime;
}
