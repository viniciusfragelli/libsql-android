package br.com.virtual.sql;

import org.junit.Test;

import java.text.SimpleDateFormat;

import br.com.virtual.sql.mysql.MySQL;
import br.com.virtual.sql.mysql.MySQLException;
import br.com.virtual.sql.mysql.annotations.SerializableException;
import br.com.virtual.sql.mysql.utils.BancoInformation;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testeAPI(){
        BancoInformation banco = new BancoInformation() {
            @Override
            public String getUser() {
                return "teste";
            }

            @Override
            public String getPass() {
                return "teste";
            }

            @Override
            public String getDNS() {
                return "localhost:3306";
            }

            @Override
            public String getBase() {
                return "teste";
            }
        };
        MySQL my = new MySQL(banco);
        try {
            my.select("SELECT times,dates,timestamps,datetime FROM teste.teste");
            my.next();
            ParseTeste p = (ParseTeste) my.getObjectVO(ParseTeste.class);
            System.out.println(p.dataToDateTime);
            System.out.println(p.dataToTimeStamp);
            System.out.println(p.date);
            System.out.println(p.dateSQL);
            System.out.println(p.times);
            System.out.println(new SimpleDateFormat("HH:mm").format(p.times));
        } catch (MySQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (SerializableException e) {
            e.printStackTrace();
        }
    }
}